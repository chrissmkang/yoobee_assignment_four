$(onReady);

function onReady(e) {

    console.log('doc ready');


    //    ----------------------------INTRO SCREEN-------------------------
    $(".logo").delay(300).fadeOut({
        complete: function () {
            $(".introScreen").fadeOut("slow");
            resumeAni();
        }
    });


    //---------------------------------NAVIGATION--------------------------------------

    //change current page to load it first
    var currentPage = $("#profile");

    var paper = $('.paper');
    paper.css({
        height: 0
    })
    var allPages = $(".paper-content");
    allPages.css({
        opacity: 0
    }).addClass('hidden');
    var navigation = $('.navigation');
    navigation.css({
        opacity: 0
    })


    function resumeAni(e) {



        paper.animate({
            height: '90vh'
        }, 1000, 'easeOutBounce', function () {
            currentPage.removeClass('hidden').animate({
                opacity: 1
            }, 500);
            navigation.animate({
                opacity: 1
            }, 1000);
        });

    }

    for (var i = 0; i < $('.nav-link').length; i++) {

        var navLink = $('.nav-link')[i];
        $(navLink).on('click', function (e) {
            //IF navigation has been clicked 

            e.preventDefault;

            var clicked_link = $(e.currentTarget).attr('href');
            //eg. resume
            var nextPage = $($('div').find(clicked_link));

            //            console.log(clicked_link);
            //            console.log("#" + nextPage["0"]["id"]);

            if (clicked_link == ("#" + nextPage["0"])) {
                return;
            } else {

                //                console.log(currentPage);
                currentPage.animate({
                    opacity: 0
                }).addClass('hidden');
                $('.tab').removeClass('active');

                nextPage.removeClass('hidden').animate({
                    opacity: 1
                });
                $(e.currentTarget).parent().addClass('active');



                currentPage = $(nextPage);
            }

        })

    }


}

//--------------Google Map API-------------------------

var map;

function initMap() {
    var myLatLng = {
        lat: -37.1051866,
        lng: 174.9614845
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROAD

    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
}
