var gulp = require('gulp');
var w3cjs = require('gulp-w3cjs');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var prefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');

var pathOf = {
    css: {
        src: 'dev/scss/*.scss',
        dest: 'prod/css/'
    },
    js: {
        src: 'dev/js/*.js',
        dest: 'prod/js/'
    },
    root: {
        src: 'dev/*.html',
        dest: './prod/'
    }
}

gulp.task('html-validate', function () {
    //using gulp-w3cjs
    return gulp.src(pathOf.root.src)
        .pipe(sourcemaps.init())
        .pipe(w3cjs())
        .pipe(w3cjs.reporter())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(pathOf.root.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('scripts', function () {
    //using gulp-concat
    return gulp.src(pathOf.js.src)
        .pipe(concat('all.js'))
        .on('error', notify.onError("Error:<%= error.message %>"))
        .pipe(gulp.dest(pathOf.js.dest))
        .pipe(browserSync.reload({
            stream: true
        }));;
})

gulp.task('browser-sync', function () {
    //using browser-sync
    browserSync.init({
        server: {
            baseDir: pathOf.root.dest
        },
        browser: ["google chrome"]
    });
})

gulp.task('sassy', function () {
    //using gulp-sass
    return gulp.src(pathOf.css.src)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            precision: 2,
            errLogToConsole: true
        }))
        .on('error', notify.onError("Error:<%= error.message %>"))
        .pipe(prefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(pathOf.css.dest))
        .pipe(browserSync.reload({
            stream: true
        }))
})

gulp.task('watch', ['browser-sync', 'html-validate', 'scripts', 'sassy'], function () {
    gulp.watch(pathOf.root.src, ['html-validate']);
    gulp.watch(pathOf.js.src, ["scripts"]);
    gulp.watch(pathOf.css.src, ["sassy"]);
});

gulp.task('default', ['watch']);
